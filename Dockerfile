FROM mhart/alpine-node:12.18.0

WORKDIR /hn-client

# COPY package.json . ./

COPY yarn.lock . ./

RUN yarn

# RUN yarn start

COPY . /hn-client/

EXPOSE 3000

CMD ["yarn", "start"]