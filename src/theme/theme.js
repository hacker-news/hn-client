import { createMuiTheme } from '@material-ui/core/styles';

 const theme = createMuiTheme({
  palette: {
    primary: { 
      light: "#00a8e8",
      main: '#003459',
      dark: "#00171f"
    }, 
    secondary: {
      light: '#47ffe3',
      main: '#007ea7',
      dark: "#00e0bf"
    },
    error: {
      light: '#f50000',
      main: '#cc0000',
      dark: '#a30000',
      contrastText: '#fff',
    },
  },
  typography: {
    fontFamily: 'Fredoka One'
    //fontFamily: ['Fredoka One'].join(','),
  }
});

export default theme;