import React from 'react';
import './App.css';
import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from './theme/theme'
import Home from './pages/Home';

function App() {
  return (
      <MuiThemeProvider theme={theme}>
        <div className="App">
            <Home />
        </div>
      </MuiThemeProvider>
  );
}

export default App;
