import React, {useEffect, useState} from 'react';
import dateParser from '../utils/date-parser'
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from 'moment';
const ApiURL = "http://localhost:3001/api";

// Get news from the last 3 days on init 
const dateFrom = moment().subtract(3,'d').toISOString()


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: '100%',
    maxWidth: '100%',
    backgroundColor: theme.palette.background.paper
  },
  listItem:{
    height: "60px",
    borderBottom: '1px solid #ccc',
    paddingRight: '0'
  },
  AtricleTitle:{
    display: 'inline',
    fontFamily:'Fredoka One',
    color: '#333',
    fontSize: '13px'
  },
  DeleteIcon:{
    color: theme.palette.error.main
  }
}));

async function putData(url = '', data = {}) {
  const response = await fetch(url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
  return response.json();
}

export default function CheckboxListSecondary() {
  const classes = useStyles();
  
  const [state, setState] = useState({data:[]});
  const [ listening, setListening ] = useState(false);

  useEffect(() => {
    fetch(`${ApiURL}/articles?dateFrom=${dateFrom}`)
    .then(res => res.json())
    .then(json => {
      json.forEach(el => el.created_at = dateParser(el.created_at))
      setState((prevState) => {
        return {data: json}}
        )
    })
  }, [])
  
  useEffect( () => {
    if (!listening) {
      const events = new EventSource(`${ApiURL}/refresh`);

      events.onmessage = (event) => {
        let newArticles = JSON.parse(event.data)
        newArticles.forEach(el => el.created_at = dateParser(el.created_at))

        setState(prevState => {
          console.log(prevState)
          let newData = [...newArticles, ...prevState.data]
          return {data: newData}
        })
      };
      setListening(true);
    }
  }, [listening]);

  const handleDelete= async (index) =>{
    let res = await putData(`${ApiURL}/articles`, 
    { 
      articleId:  state.data[index]._id
    });

    if(res.data){
      state.data.splice(index, 1)
      setState({data: state.data})
    }
  }
  const handleClick = (index)=>{
    let openTab= url => window.open(url, '_blank');

    state.data[index].url 
    ? openTab(state.data[index].url) 
    : openTab(state.data[index].story_url)
  }

  if(state.data.length === 0)
  return <div style={{display: 'flex', justifyContent: 'center'}}><CircularProgress /></div>

  return (
    <List className={classes.root}>
        {state.data.map((value, index) => {
          return (
            <ListItem
            onClick={()=>handleClick(index)}
            className={classes.listItem}
            key={index} button>
              <Grid item xs={8}>
                <ListItemText disableTypography 
                className={classes.AtricleTitle} 
                primary={` ${ state.data[index].story_title ? state.data[index].story_title : state.data[index].title } `} />
                <ListItemText disableTypography 
                style={{color: '#999'}} className={classes.AtricleTitle} 
                primary={` -${ state.data[index].author } -`} />
              </Grid>
              <ListItemText disableTypography 
              className={classes.AtricleTitle} 
              primary={` ${ state.data[index].created_at }` }/>
              
              <ListItemSecondaryAction >
                <IconButton
                  onClick={()=> {handleDelete(index)}}
                  edge="end" 
                  aria-label="delete">
                    <DeleteIcon className={classes.DeleteIcon}/>
                </IconButton >
              </ListItemSecondaryAction>
            </ListItem>
          );  
        })}
      </List>
  );
}
