import moment from 'moment';

export default function dateParser(date){
  date = moment(date);
  let today = moment()
  const diffHours = today.diff(date, 'hours');
  date = moment(date).format('LLLL')
  let hoursFromToday = parseInt(moment().format('HH'));
  if(diffHours < hoursFromToday)
  {
    date = date.slice(-8).toLowerCase()
    return date
  }
  else if(
    diffHours >= hoursFromToday
   && 
  diffHours < hoursFromToday + 24
  )
  {
    date = "yesterday"
    return date
  }
  else{
    date = date.split(',')
    date = date[1]
    return date
  }
}