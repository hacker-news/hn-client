import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import AppBar from '../components/app-bar/app-bar';
import Articles from '../components/articles/articles';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    margin: '10px 0'
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center',
    color: theme.palette.text.secondary,
    height: '100%'
  }
}));

export default function StudyPage() {
  const classes = useStyles();

  return (
    <div>
      <AppBar />
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
              <Articles />
          </Grid>
        </Grid>
      </div>
    </div>
  );
}